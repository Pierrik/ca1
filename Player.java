import java.util.ArrayList;
import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicInteger;

public class Player extends Loggable implements Runnable {
    public static boolean isUnitTesting = false;

    private final int playerNumber;
    private final ArrayList<Card> hand = new ArrayList<>();
    private Deck pickupDeck;
    private Deck putDownDeck;
    private AtomicInteger turnsPlayed = new AtomicInteger(0);

    private AtomicInteger winner = new AtomicInteger(0);
    private CountDownLatch allPlayersHaveSetMaxTurns = new CountDownLatch(1);
    private AtomicInteger maxTurnsPlayed = new AtomicInteger(0);

    /**
     * Simplest constructor for player.
     * @param playerNumber: an integer representing the id number of the player.
     */
    public Player(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    /**
     * @param playerNumber: an integer representing the id number of the player.
     * @param pickupDeck: the deck from which the player picks up cards.
     */
    public Player(int playerNumber, Deck pickupDeck) {
        this(playerNumber);

        if (pickupDeck == null) throw new RuntimeException("pickupDeck cannot be null.");
        this.pickupDeck = pickupDeck;

    }

    /**
     * @param playerNumber: an integer representing the id number of the player.
     * @param pickupDeck: the deck from which the player picks up cards.
     * @param winner: the pointer to an AtomicInteger which is shared between multiple players representing the id of the winning player.
     * @param maxTurnsPlayed: the pointer to an AtomicInteger which is shared between multiple players representing the maximum number of turns played by any player.
     */
    public Player(int playerNumber, Deck pickupDeck, AtomicInteger winner, AtomicInteger maxTurnsPlayed) {
        this(playerNumber, pickupDeck);

        if (winner == null) throw new RuntimeException("winner cannot be null.");
        if (maxTurnsPlayed == null) throw new RuntimeException("maxTurnsPlayed cannot be null.");

        this.winner = winner;
        this.maxTurnsPlayed = maxTurnsPlayed;
    }

    /**
     * @param playerNumber: an integer representing the id number of the player.
     * @param pickupDeck: the deck from which the player picks up cards.
     * @param winner:
     *              the pointer to an AtomicInteger which is shared between multiple players representing the id of the
     *              winning player.
     * @param maxTurnsPlayed:
     *              the pointer to an AtomicInteger which is shared between multiple players representing the maximum
     *              number of turns played by any player.
     * @param allPlayersHaveSetMaxTurns:
     *              a shared CountDownLatch which keeps track of how many players have set the
     *              maximum number of turns played and allows them to wait until all players have tried to set it.
     */
    public Player(int playerNumber, Deck pickupDeck, AtomicInteger winner, AtomicInteger maxTurnsPlayed,  CountDownLatch allPlayersHaveSetMaxTurns) {
        this(playerNumber, pickupDeck, winner, maxTurnsPlayed);

        if (allPlayersHaveSetMaxTurns == null) throw new RuntimeException("allPlayersHaveSetMaxTurns cannot be null.");
        this.maxTurnsPlayed = maxTurnsPlayed;
    }

    /**
     * Adds a card to the players hand, checks for an initial win, and sets the shared winner variable accordingly.
     * @param card: card to be added
     */
    public void giveCard(Card card) {
        if (card == null) throw new RuntimeException("Cannot give null card to player.");
        hand.add(card);
        trySetWinner();
    }

    // Run breakdown methods
    /**
     * Logs correct winner information depending on whether the current player is the winner.
     */
    public void logWinner () {
        if (getWinner() == playerNumber) {
            if (!Player.isUnitTesting) {
                System.out.printf("Player %d has won%n", playerNumber);
            }

            log(String.format("player %d wins.",
                    playerNumber));
        } else {
            log(String.format("player %d has informed player %d that player %d has won.",
                    getWinner(), playerNumber, getWinner()));
        }
    }

    /**
     * Updates the maximum number of turns played if necessary and waits for the other players to have done the same.
     */
    public void calculateMaxTurnsPlayed () {
        // Someone has won, so try and set max turns played
        if (getTurnsPlayed() > maxTurnsPlayed.get()) {
            maxTurnsPlayed.set(getTurnsPlayed());
        }

        // Keep track of how many players have tried to set a maximum number of turns played
        // Keep waiting until all players have tried to set max turns played
        try {
            allPlayersHaveSetMaxTurns.countDown();
            allPlayersHaveSetMaxTurns.await();
        } catch (InterruptedException e) { }
    }

    /**
     * Log whether the player needs to play catch up turns or not.
     */
    public void logCatchUpTurns() {
        if (getTurnsPlayed() < getMaxTurnsPlayed()) {
            log("starting catch up turns.\n");
        } else {
            log("no catch up turns to be played.\n");
        }
    }

    /**
     * Logs end-of-game lines according to spec.
     */
    public void logFinalHand() {
        log(String.format("player %d exits.", playerNumber));

        if (getWinner() == playerNumber) {
            log(String.format("player %d final hand: %s.",
                    playerNumber, printHand()));
        } else {
            log(String.format("player %d hand: %s.",
                    playerNumber, printHand()));
        }
    }

    /**
     * Keeps calling playTurn until condition evaluates to true.
     * @param condition: a lambda function which returns a boolean used to stop the while loop.
     */
    public void playTurnsUntil(Callable<Boolean> condition) {
        try {
            while (condition.call()) {
                try {
                    this.playTurn();
                } catch (Exception e) {
                    // PickUp deck is empty so try to give CPU time to other threads.
                    Thread.yield();
                }
            }
        } catch (Exception e) {}

    }

    /**
     * Method run by the thread during its life cycle.
     */
    public void run() {
        log(String.format("player %d initial hand %s.", playerNumber, printHand()));

        // Play as many turns as possible until someone wins
        playTurnsUntil(() -> winner.get() == 0);

        logWinner();
        calculateMaxTurnsPlayed();
        logCatchUpTurns();

        // Keep playing turns until all players have played the same number of turns
        playTurnsUntil(() -> getTurnsPlayed() < getMaxTurnsPlayed());

        logFinalHand();
    }


    /**
     * Plays 1 turn for the current player:
     *      Pick up card
     *      Decide which card to remove according to the player's strategy (keep cards with same value as their playerNumber)
     *      Put down card
     *      Try and set the winner if they have won
     * @throws Exception if the pickup deck is empty so the turn cannot be played.
     */
    public void playTurn() throws Exception {
        try {
            Card pickedUp = pickupDeck.pickupCard();
            hand.add(pickedUp);
            log(String.format("player %d draws a %d from deck %d.",
                    playerNumber, pickedUp.getValue(), pickupDeck.getDeckNumber()));

            // Apply player strategy
            ArrayList<Card> potentialRemove = new ArrayList<>();

            for (Card card : hand) {
                if (card.getValue() != playerNumber) {
                    potentialRemove.add(card);
                }
            }

            if (potentialRemove.size() > 0) {
                // Select a random card to remove so game does not stagnate
                int num = ThreadLocalRandom.current().nextInt(0, potentialRemove.size());

                Card cardToPutDown = potentialRemove.get(num);

                hand.remove(cardToPutDown);
                putDownDeck.putDownCard(cardToPutDown);
                log(String.format("player %d discards a %d to deck %d.",
                        playerNumber, cardToPutDown.getValue(), pickupDeck.getDeckNumber()));
            } else {
                // Handle the case that a player has winning hand but needs to keep playing turns to catch up
                hand.remove(pickedUp);
                putDownDeck.putDownCard(pickedUp);
            }


            log(String.format("player %d current hand is %s.", playerNumber, printHand()));

            turnsPlayed.incrementAndGet();
            trySetWinner();
        } catch (NullPointerException e) {
            throw new Exception("Pickup deck is empty.");
        }
    }


    /**
     * @return true if the player has a winning hand, otherwise false.
     */
    public boolean hasWon () {
        if (getHand().size() != 4) {
            return false;
        }

        Card previousCard = null;

        for (Card card : hand) {
            // Check that card is the same as previous card.
            if (previousCard != null && card.getValue() != previousCard.getValue() ) {
                return false;
            }
            previousCard = card;
        }

        return true;
    }

    /**
     * Sets the winner variable if the current player has won and nobody has already won.
     */
    public void trySetWinner() {
        if (winner.get() == 0 && hasWon()) {
            winner.set(playerNumber);
        }
    }


    public void writeFile(String outputDirectory) {
        super.writeFile(String.format(outputDirectory + "players/player%d_output.txt", playerNumber));
    }

    public String toString () {
        return  "Player " + playerNumber;
    }

    /**
     * @return a String representing the values of all the cards in the player's hand.
     */
    public String printHand() {
        StringBuilder allCards = new StringBuilder();
        for (Card card : hand) {
            allCards.append(card.getValue()).append(" ");
        }
        return allCards.toString().trim();
    }


    // Getters
    public int getTurnsPlayed() {
        return turnsPlayed.get();
    }

    public ArrayList<Card> getHand() {
        return hand;
    }

    public int getPlayerNumber () {
        return playerNumber;
    }

    public int getWinner () {
        return winner.get();
    }

    public int getMaxTurnsPlayed () {
        return maxTurnsPlayed.get();
    }

    // Setters
    public void setPutDownDeck(Deck putDownDeck) {
        this.putDownDeck = putDownDeck;
    }

    public void setWinner(AtomicInteger winner) {
        this.winner = winner;
    }

    public void setMaxTurnsPlayed(AtomicInteger maxTurnsPlayed) {
        this.maxTurnsPlayed = maxTurnsPlayed;
    }
}