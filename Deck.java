import java.util.concurrent.ConcurrentLinkedQueue;

public class Deck extends Loggable {
    private final int deckNumber;
    private ConcurrentLinkedQueue<Card> cards = new ConcurrentLinkedQueue<>();

    /**
     * Constructor for deck.
     * @param deckNumber an integer representing the id number of the deck.
     */
    public Deck(int deckNumber) {
        this.deckNumber = deckNumber;
    }

    /**
     * Adds a card to the bottom of the deck.
     * @param card: the card to add.
     */
    public void putDownCard(Card card) {
        cards.add(card);
    }

    /**
     * Gets the top card of the deck and returns it.
     * @return the top card of the deck.
     * @throws NullPointerException if the deck is empty.
     */
    public Card pickupCard() throws NullPointerException{
        Card pickedUpCard = cards.poll();

        if (pickedUpCard == null) throw new NullPointerException("Deck is empty");

        return pickedUpCard;
    }


    // Getters
    public int getDeckNumber (){
        return deckNumber;
    }

    public ConcurrentLinkedQueue<Card> getCards() {
        return cards;
    }


    // Utility methods
    /**
     * @return a string displaying all the cards currently in the deck.
     */
    public String printDeck() {
        StringBuilder allCards = new StringBuilder();
        for (Card card : cards) {
            allCards.append(card.getValue()).append(" ");
        }
        return allCards.toString().trim();
    }

    /**
     * Adds a line to the log with the current deck number and cards in the deck.
     */
    public void logDeck () {
          log(String.format("deck %d contents: %s", getDeckNumber(), printDeck() ));
    }

    /**
     * Writes the log to the given outputDirectory
     * @param outputDirectory a String representing the name of the desired directory for the output files.
     */
    public void writeFile(String outputDirectory) {
        super.writeFile(String.format(outputDirectory + "decks/deck%d_output.txt", getDeckNumber()));
    }
}