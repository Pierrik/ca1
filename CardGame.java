import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import static java.util.concurrent.Executors.newCachedThreadPool;


public class CardGame {
    private final int numberOfPlayers;
    private final String OUTPUT_DIRECTORY = "output_files/";
    private final ArrayList<Player> players = new ArrayList<>();
    private final ArrayList<Deck> decks = new ArrayList<>();

    private final ExecutorService threadPool = newCachedThreadPool();

    private AtomicInteger winner = new AtomicInteger(0);
    private AtomicInteger maxTurnsPlayed = new AtomicInteger(0);
    private CountDownLatch allPlayersHaveSetMaxTurns;

    private Boolean hasCreatedPlayersAndDeck = false;
    private Boolean hasDistributedToPlayers = false;
    private Boolean hasDistributedToDecks = false;


    /**
     * Constructor
     * Creates players and decks
     * Distributes the pack to the players and decks according to spec
     * @param numberOfPlayers: integer representing the number of players in the game
     * @param pack: an array list of cards to be used in the game
     */
    public CardGame(int numberOfPlayers, ArrayList<Card> pack) {

        if (pack == null) throw new RuntimeException("Pack cannot be null");

        if (numberOfPlayers < 1) throw new RuntimeException("Number of players must be >= 1");

        this.numberOfPlayers = numberOfPlayers;
        this.allPlayersHaveSetMaxTurns = new CountDownLatch(numberOfPlayers);

        this.createPlayersAndDecks();


        this.playerDistribution(pack);
        this.deckDistribution(pack);
    }

    /**
     * Adds the threads to the thread pool, starting them
     */
    public void start() {
        for (Player player : getPlayers()) {
            threadPool.execute(player);
        }
    }

    /**
     * Reads and validates a pack from a txt file.
     * @param fileName: name of file to read pack from
     * @param numberOfPlayers: number of players in the game
     * @return: an array list of cards that were in the pack file
     * @throws Exception: if pack is invalid
     */
    public static ArrayList<Card> readPack(String fileName, int numberOfPlayers) throws Exception {
        ArrayList<Card> pack = new ArrayList<>();

        File file = new File(fileName);

        BufferedReader br = new BufferedReader(new FileReader(file));

        String st;
        while ((st = br.readLine()) != null) {
            pack.add(new Card(st));
        }

        br.close();

        if (pack.size() != 8 * numberOfPlayers) {
            throw new Exception(String.format("Invalid pack length, should be of size %d, got %d", 8 * numberOfPlayers, pack.size()));
        }

        return pack;
    }

    /**
     * Creates the players and decks
     * Sets the put down deck of the previous player as the pickup deck of the next player
     */
    void createPlayersAndDecks() {
        // Prevents a game from creating players and decks multiple times
        if (hasCreatedPlayersAndDeck) {
            return;
        }

        Player prevPlayer = null;
        for (int i = 1; i <= numberOfPlayers; i++) {
            Deck newDeck = new Deck(i);
            getDecks().add(newDeck);

            // Try and set the previous player's putDown deck
            if (prevPlayer != null) prevPlayer.setPutDownDeck(newDeck);

            Player newPlayer = new Player(i, newDeck, winner, maxTurnsPlayed, allPlayersHaveSetMaxTurns);
            getPlayers().add(newPlayer);

            prevPlayer = newPlayer;
        }

        // Final link between the final player and first put down deck
        prevPlayer.setPutDownDeck(getDecks().get(0));

        hasCreatedPlayersAndDeck = true;
    }

    /**
     * Distributes the cards in the pack to the players according to the spec (round robin)
     * Removes the card from the pack once it has been distributed to a player
     * @param pack: array list containing the cards to distribute amongst the players
     */
    public void playerDistribution(ArrayList<Card> pack) {
        if (hasDistributedToPlayers) {
            return;
        }

        for (int i = 0; i < 4*numberOfPlayers; i++) {
            Card card = pack.get(i);
            players.get(i % numberOfPlayers).giveCard(card);
        }

        for (int i = 0; i < 4*numberOfPlayers; i++) {
            pack.remove(i);
        }

        hasDistributedToPlayers = true;
    }

    /**
     * Distributes the cards in the pack to the decks according to the spec
     * @param pack: array list containing the cards to distribute amongst the decks
     */
    public void deckDistribution(ArrayList<Card> pack) {
        if (hasDistributedToDecks) {
            return;
        }

        int count = 0;
        for (Card card: pack) {
            decks.get(count % numberOfPlayers).putDownCard(card);
            count += 1;
        }

        hasDistributedToDecks = true;
    }

    /**
     * Create the output directory to host output files
     * Create an output file for all players and decks
     * Writes in the games data for the players and decks
     */
    public void writeFiles (String outputDirectory) {
        // Delete output directory
        try {
            Files.walk(Paths.get(outputDirectory))
                    .sorted(Comparator.reverseOrder())
                    .map(Path::toFile)
                    .forEach(File::delete);
        } catch (IOException e) { }

        // Write files
        for (Player player : players) {
            player.writeFile(outputDirectory);
        }
        for (Deck deck : decks) {
            deck.logDeck();
            deck.writeFile(outputDirectory);
        }
    }

    /**
     * Shutsdown the game if the program has ran for more than timeout
     * @param timeout: amount of time program is allowed to run for
     * @param unit: unit of time that timeout represents
     * @throws InterruptedException: thrown is program runs for more than timeout time
     */
    public void awaitEndOfGame(int timeout, TimeUnit unit) throws InterruptedException{
        threadPool.shutdown();
        threadPool.awaitTermination(timeout, unit);
    }

    // Getters
    public ArrayList<Player> getPlayers() {
        return players;
    }

    public ArrayList<Deck> getDecks() {
        return decks;
    }

    public static void main(String[] args) {

//        Get number of players from user
        Integer n = null;
        Scanner scanner = new Scanner(System.in);

        while (n == null) {
            System.out.print("Please enter the number of players: \n");
            String input = scanner.nextLine();
//            Validate input
            try {
                int integer = Integer.parseInt(input);
                if (integer < 1) {
                    throw new Exception("Integer must be greater than 0.");
                } else {
                    n = integer;
                }
            } catch (Exception e) {
                System.out.println("Invalid input, please enter an integer >= 1");
            }
        }

//        Get input pack from player and validate
        ArrayList<Card> pack = null;
        while (pack == null) {
            System.out.print("Please enter the file path of an input pack: \n");
            String input = scanner.nextLine();
//            Validate file input
            try {
                pack = CardGame.readPack(input, n);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        CardGame game = new CardGame(n, pack);
        game.start();
        System.out.println("Game started.");
        try {
            game.awaitEndOfGame(1,  TimeUnit.HOURS);
        } catch (InterruptedException e) { }

        System.out.println("Game ended, writing files...");
        game.writeFiles(game.OUTPUT_DIRECTORY);
        System.out.println("Output files written.");
    }
}