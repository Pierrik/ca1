public class Card {

    private final int value;

    /**
     * Constructs a new card and checks that the value of the card is positive integer.
     * @param stringValue: a string representing the desired card value.
     * @throws Exception: if card value is invalid.
     */
    public Card(String stringValue) throws Exception {
        int potentialValue;

        if (!stringValue.matches("^\\d+$")) throw new Exception("Invalid card value");

        // Parse the input string
        potentialValue = Integer.parseInt(stringValue);


        if (potentialValue < 1) throw new Exception(String.format("Card must have a non-zero positive integer value, got \"%d\"", potentialValue));
        this.value = potentialValue;
    }

    public int getValue() {
        return value;
    }

    @Override
    public String toString() {
        return "Card value " + getValue();
    }
}