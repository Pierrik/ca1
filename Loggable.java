import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * A class to be extended by other classes which allows them to store log history and write it to a file.
 */
public class Loggable {

    private ConcurrentLinkedQueue<String> history = new ConcurrentLinkedQueue<>();

    /**
     * Add a line to the log history.
     * @param line: a string representing the line to be added.
     */
    public void log (String line) {
        history.add(line);
    }

    /**
     * Write the whole log history on separate lines.
     * @param fileName: name of the file to write to.
     */
    public void writeFile (String fileName) {
        File file = new File(fileName);

        // Create the parent directories in the path of the output file if necessary
        File parent = file.getParentFile();
        if (parent != null) {
            parent.mkdirs();
        }

        // Write the lines in the history to the file with a newline in between
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file));
            for (String line : history) {
                bw.write(line);
                bw.newLine();
            }
            bw.close();
        } catch (IOException e) {}
    }

    public ConcurrentLinkedQueue<String> getHistory() {
        return history;
    }
}